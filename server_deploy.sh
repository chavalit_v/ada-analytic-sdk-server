#! /bin/sh

LOCAL_HOST=`bash ip_address.sh 2> /dev/null`
SERVER_HOST=${1:-$LOCAL_HOST}

if [[ -z "$SERVER_HOST" ]];
then
	SERVER_HOST='http://localhost'
fi

INDEX_REQ_URL="$SERVER_HOST:9200/ada_analytic_logs"
MAPPING_REQ_URL="$SERVER_HOST:9200/ada_analytic_logs/_mapping/event"

echo $INDEX_REQ_URL

INDEX_CURL_RESPONSE=`curl -vX PUT $INDEX_REQ_URL \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H 'Cache-Control: no-cache' \
-o /dev/null \
-w '%{http_code}' \
-d '
{
	"settings": {
		"index": {
			"number_of_shards": "5",
			"number_of_replicas": "1"
		}
	},
	"defaults": {
		"index": {
			"max_inner_result_window": "100",
			"unassigned": {
				"node_left": {
					"delayed_timeout": "1m"
				}
			},
			"max_terms_count": "65536",
			"lifecycle": {
				"name": "",
				"rollover_alias": "",
				"indexing_complete": "false"
			},
			"routing_partition_size": "1",
			"max_docvalue_fields_search": "100",
			"merge": {
				"scheduler": {
					"max_thread_count": "2",
					"auto_throttle": "true",
					"max_merge_count": "7"
				},
				"policy": {
					"reclaim_deletes_weight": "2.0",
					"floor_segment": "2mb",
					"max_merge_at_once_explicit": "30",
					"max_merge_at_once": "10",
					"max_merged_segment": "5gb",
					"expunge_deletes_allowed": "10.0",
					"segments_per_tier": "10.0",
					"deletes_pct_allowed": "33.0"
				}
			},
			"max_refresh_listeners": "1000",
			"max_regex_length": "1000",
			"load_fixed_bitset_filters_eagerly": "true",
			"number_of_routing_shards": "5",
			"write": {
				"wait_for_active_shards": "1"
			},
			"mapping": {
				"coerce": "false",
				"nested_fields": {
					"limit": "50"
				},
				"depth": {
					"limit": "20"
				},
				"ignore_malformed": "false",
				"total_fields": {
					"limit": "1000"
				}
			},
			"source_only": "false",
			"soft_deletes": {
				"enabled": "false",
				"retention": {
					"operations": "0"
				}
			},
			"max_script_fields": "32",
			"query": {
				"default_field": [
				"*"
				],
				"parse": {
					"allow_unmapped_fields": "true"
				}
			},
			"format": "0",
			"frozen": "false",
			"sort": {
				"missing": [],
				"mode": [],
				"field": [],
				"order": []
			},
			"priority": "1",
			"codec": "default",
			"max_rescore_window": "10000",
			"max_adjacency_matrix_filters": "100",
			"gc_deletes": "60s",
			"optimize_auto_generated_id": "true",
			"max_ngram_diff": "1",
			"translog": {
				"generation_threshold_size": "64mb",
				"flush_threshold_size": "512mb",
				"sync_interval": "5s",
				"retention": {
					"size": "512mb",
					"age": "12h"
				},
				"durability": "REQUEST"
			},
			"auto_expand_replicas": "false",
			"mapper": {
				"dynamic": "true"
			},
			"requests": {
				"cache": {
					"enable": "true"
				}
			},
			"data_path": "",
			"highlight": {
				"max_analyzed_offset": "-1"
			},
			"routing": {
				"rebalance": {
					"enable": "all"
				},
				"allocation": {
					"enable": "all",
					"total_shards_per_node": "-1"
				}
			},
			"search": {
				"slowlog": {
					"level": "TRACE",
					"threshold": {
						"fetch": {
							"warn": "-1",
							"trace": "-1",
							"debug": "-1",
							"info": "-1"
						},
						"query": {
							"warn": "-1",
							"trace": "-1",
							"debug": "-1",
							"info": "-1"
						}
					}
				},
				"throttled": "false"
			},
			"fielddata": {
				"cache": "node"
			},
			"default_pipeline": "_none",
			"max_slices_per_scroll": "1024",
			"shard": {
				"check_on_startup": "false"
			},
			"xpack": {
				"watcher": {
					"template": {
						"version": ""
					}
				},
				"version": "",
				"ccr": {
					"following_index": "false"
				}
			},
			"percolator": {
				"map_unmapped_fields_as_text": "false",
				"map_unmapped_fields_as_string": "false"
			},
			"allocation": {
				"max_retries": "5"
			},
			"refresh_interval": "1s",
			"indexing": {
				"slowlog": {
					"reformat": "true",
					"threshold": {
						"index": {
							"warn": "-1",
							"trace": "-1",
							"debug": "-1",
							"info": "-1"
						}
					},
					"source": "1000",
					"level": "TRACE"
				}
			},
			"compound_format": "0.1",
			"blocks": {
				"metadata": "false",
				"read": "false",
				"read_only_allow_delete": "false",
				"read_only": "false",
				"write": "false"
			},
			"max_result_window": "10000",
			"store": {
				"stats_refresh_interval": "10s",
				"type": "",
				"fs": {
					"fs_lock": "native"
				},
				"preload": []
			},
			"queries": {
				"cache": {
					"enabled": "true"
				}
			},
			"ttl": {
				"disable_purge": "false"
			},
			"warmer": {
				"enabled": "true"
			},
			"max_shingle_diff": "3",
			"query_string": {
				"lenient": "false"
			}
		}
	}
}
'`

echo $INDEX_CURL_RESPONSE

if [[ $INDEX_CURL_RESPONSE -eq 200 ]];
then
	echo 'Index ada_analytic_logs created'

	MAPPING_CURL_RESPONSE=`curl -vX PUT $MAPPING_REQ_URL \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-H 'Cache-Control: no-cache' \
	-o /dev/null \
	-w '%{http_code}' \
	-d '
	{
		"properties": {
			"app_id": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"carrier": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"connection_type": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"device_model": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"event_id": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"installed_date": {
				"type": "date"
			},
			"location": {
				"type": "geo_point"
			},
			"maid": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"os_version": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"platform": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"session_id": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"timestamp": {
				"type": "date"
			}
		}
	}
	'`

	echo $MAPPING_CURL_RESPONSE

	if [[ $MAPPING_CURL_RESPONSE -eq 200 ]];
	then
		echo 'Mapping event created'
	else
		echo 'Failed to create Mapping event'
	fi

else
	echo 'Failed to create Index ada_analytic_logs'
fi 